#code to plot synchrotron spectrum with cooling

import numpy as np
import matplotlib.pyplot as plt
plt.rc('lines', linewidth=3, color='r')
plt.rcParams.update({'font.size': 16})

nu = np.logspace(2,10,100)
Fnu_nocool = (nu/2.7e3)**-0.6
Fnu_src = 1./((nu/2.7e3)**0.6 + (nu/2.7e3)**1.1)
Fnu_nosrc =  1./((nu/2.7e3)**0.6 + (nu/2.7e3)**200)
plt.loglog(nu,Fnu_nocool,'--',label='no cooling')
plt.loglog(nu,Fnu_src, label='continuous injection & cooling')
plt.loglog(nu,Fnu_nosrc, '-.', label='only cooling of initial electrons')
plt.xlabel(r'$\nu$(Hz)')
plt.ylabel('flux (arbitrary units)')
plt.axis([60,2e9,2e-7,20])
plt.text(1e7, 1e-2, r'$\propto \nu^{-0.6}$')
plt.text(1e7, 1e-4, r'$\propto \nu^{-1.1}$')
plt.legend()
plt.show()
plt.grid()