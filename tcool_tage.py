import numpy as np
import matplotlib.pyplot as plt
plt.rc('lines', linewidth=3, color='r')
plt.rcParams.update({'font.size': 18})

num = 100
B = np.logspace(-7,0,num)
tage = 180.*np.ones(num)
tage_now = 13.6e3*np.ones(num)
dtedges = 3.*tage/8.
tsync = 1.6*(B/1.e-4)**-1.5
tIC_R = 0.014*(B/1.e-4)**.5
tIC_R_now = 0.014*(B/1.e-4)**.5*18.**4
t_cool = 1./(1./tIC_R + 1./tsync)
#plt.loglog(B,tage,'-',label=r'$t_{\rm age}$, z=17')
plt.loglog(B,dtedges,'-.',label=r'$\Delta t_{\rm EDGES}$')
plt.loglog(B,tsync,'--',label=r'$t_{\rm syn}$')
plt.loglog(B,tIC_R,':',label=r'$t_{\rm IC}$, z=17')
plt.loglog(B, t_cool, '-', label=r'$t_{\rm cool}$, z=17')
#plt.loglog(B,tage_now,'-',label=r'$t_{\rm age}$, z=0')
plt.loglog(B,tIC_R_now,':',label=r'$t_{\rm IC}$, z=0')
plt.xlabel(r'$B$(G)', fontsize=18)
plt.ylabel(r'timescales (Myr)',fontsize=18)
plt.legend(loc=1)
plt.fill([5e-7, 1e-4, 1e-4, 5e-7],[8e-3, 8e-3, 8e3, 8e3],alpha=0.2)
plt.text(1e-6, 1, 'SXB constraint', fontsize=16)
plt.axis([5.e-7,0.5,8.e-3,8e3])
plt.grid()
plt.show()